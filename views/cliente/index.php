<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir cliente', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div style="background-color: #FCF3CF">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dni',
            'direccion',
            'codigo_cliente',
           
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    </div>

</div>
