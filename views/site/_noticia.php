<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

$this->title = "Todas las noticias";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="col-md-4">
    <div class="thumbnail noticias" style="max-height: 900px;">
        <div class="caption" style="border-style: solid">
            
            <div style="margin:11px;" align="center">
                
                <h3><?= "". $model->titulo ?></h3>
                
            </div>
             
            <div style="margin:40px;" align="justify">
                <?= "". $model->cuerpo ?>
            </div>
                
                 
            <div align="center">
                <?= Html::img($model->imagen, ['alt' => 'My logo']) ?>
            </div>
            
        </div>
    </div>
</div>