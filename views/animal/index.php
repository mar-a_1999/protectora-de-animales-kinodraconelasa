<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Animales';
$this->params['breadcrumbs'][] = $this->title;

?>
    
<div class="animal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir animal', ['create'], ['class' => 'btn btn-success']) ?> 
    </p>

    
    <div style="background-color: #FCF3CF">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{items}",
        'columns' => [
        
            ['class' => 'yii\grid\SerialColumn'], 

            'nombre',
            'codigo_animal',
            'codigo_local',
            'raza',
            [

             'label'=>'Está vacunado',

             'format'=>'raw',

             'value' => function($model, $key, $index, $column) { return $model->esta_vacunado == false ? 'No' : 'Sí';},

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    ?>
    </div>    
    
</div>