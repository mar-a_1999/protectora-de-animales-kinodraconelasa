<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<?php $this->beginBody() ?> 
    
<body style="background-color:#e8daef;">

    <header style="background-color: white;">
            
        <p align="center"><a href="javascript:history.back();"><?= Html::img('@web/images/logotipo_protectora_de_animales.JPG', ['alt' => 'Error al mostrar la imagen'])?></a></p>
    
    </header>
    
<div class="wrap">

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>



<footer style="background-color:#34495e">
    <div class="container" style="background-color:#34495e">
         
        <p align="center" style="color:white">Realizado por María de Andrés García, alumna de segundo de DAM (Desarrollo de aplicaciones multiplataforma.)</p>
       
        <p align="center" style="color:white"><?= Yii::powered() ?></p>
         
    </div>
</footer>
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>