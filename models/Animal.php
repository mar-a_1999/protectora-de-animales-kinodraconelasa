<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Animal".
 *
 * @property int $codigo_animal
 * @property int|null $codigo_local
 * @property string|null $nombre
 * @property string|null $raza
 * @property int|null $esta_vacunado
 */
class Animal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Animal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_local', 'esta_vacunado'], 'integer'],
            [['nombre', 'raza'], 'string', 'max' => 30],
            [['codigo_local'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['codigo_local' => 'codigo_local']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_animal' => 'Codigo Animal',
            'codigo_local' => 'Codigo Local',
            'nombre' => 'Nombre',
            'raza' => 'Raza',
            'esta_vacunado' => 'Esta Vacunado',
        ];
    }
}
