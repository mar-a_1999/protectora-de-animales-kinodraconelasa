<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir empleado', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div style="background-color: #FCF3CF">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'apellidos',
            'edad',
            'dni',
            'codigo_empleado',
            'codigo_local',
            //'dni',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    ?>
    </div>

</div>
