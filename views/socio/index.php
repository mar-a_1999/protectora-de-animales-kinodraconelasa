<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Socios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="socio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir socio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <div style="background-color: #FCF3CF">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'dni',
            'direccion',
            'codigo_socio',
            'codigo_empleado',
            'codigo_local',
            
            //'direccion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    ?>
    </div>
    
</div>
