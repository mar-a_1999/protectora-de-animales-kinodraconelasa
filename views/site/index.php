<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Protectora de animales Kinodraconelasa';

?>

<html>
    <head>
    
        <style>
            
            #table1 {
               border-collapse: separate;
               border-spacing: 50px;
            } 
            
        </style>
            
    </head>
    
    <body>

<nav>
    
    <div class="body-content">

                <div class="row" style="border-style: double">
        
                    <div align="center" border-style="double">
        
                             <div class="col-lg-4" style="background-color: pink">
                             <p></p>
                             <p><?= Html::a('¿Quiénes somos?', ['site/quienessomos'])?></p>
                             </div>
                            
                             <div class="col-lg-4" style="background-color: pink">
                             <p></p>
                             <p><?= Html::a('Adoptados', ['site/adoptados'])?></p>
                             </div>
                            
                            <div class="col-lg-4" style="background-color: pink">
                            <p></p>
                            <p><?= Html::a('Todas las noticias', ['site/mostrarnoticias'])?></p>
                            </div>
                            
                       </div>
        
               </div>
        
           </div>
    
</nav>

<table id="table1">
    
    <tr>
        
        <td>
    
            <div class="body-content" style="background-color: #A9CCE3">

        <div style="border-style: double">
                
     <br>
        
     <div class="col-lg-15">
     <p align="center"><?= Html::a('Consultar información de los animales', ['animal/index'], ['class' => 'btn btn-primary'])?></p>
    </div>
      <br>
      <div class="col-lg-15">
     <p align="center"><?= Html::a('Consultar información de los clientes', ['cliente/index'], ['class' => 'btn btn-primary'])?></p>
      </div>
      <br>
      <div class="col-lg-15">
     <p align="center"><?= Html::a('Consultar información de los empleados', ['empleado/index'], ['class' => 'btn btn-primary'])?></p>
      </div>
      <br>
      <div class="col-lg-15">
     <p align="center"><?= Html::a('Consultar información de los socios', ['socio/index'], ['class' => 'btn btn-primary'])?></p>
      </div> 
     <br>
      <div class="col-lg-15">
     <p align="center"><?= Html::a('Consultar noticias', ['noticia/index'], ['class' => 'btn btn-primary'])?></p>
      </div>
     <br>
      
            </div>
        
        </div>

        </td>
        
    
    <td>
             
             <div style="border-style: double; background-color: #ffe8a1;" class="col-lg-12">
                 
                 <div class="body-content" style="background-color: #ffe8a1">
                     
                     <br>
                     
            <div style="color:#0a73bb" align="justify">
                <h3><b><i>ÚLTIMAS NOTICIAS</i></b></h3>
            </div>
               
                <br>
                <h3 align="center" style="background-color: #ffe8a1">¡Toby ha sido adoptado!</h3>
                <br>
                <p align="center" style="background-color: #ffe8a1">En la tarde de ayer, día ocho de abril, una chica de veintiún años y llamada Annabeth se presentó en nuestras instalaciones de la calle Lamara.</p> <p align="center" style="background-color: #ffe8a1">Se mostró muy interesada por nuestro cachorro, y al cabo de un rato hablando con uno de nuestros empleados, se decantó por acoger al pequeño.</p> <p align="center" style="background-color: #ffe8a1">Después de unos trámites nuestro querido amigo ya puede disfrutar de su nueva familia.</p>
                <br>
                <p align="center" style="background-color: #ffe8a1"><img src="/ProtectoraAnimalesKinodraconelasaFinDeCiclo/web/images/perro_adoptado_ pagina_principal.jpg" alt="Error al mostrar la imagen"></p>
                
                <br>
                
            </div>
                
            </div>
                 
               
            
        </td>
    <br>
</tr>

<br>

<tr>
    
    <td>
        
         <div class="body-content">

        <div class="pull-right" style="border-style: double">
            
            <div class="col-lg-12" style="background-color: #D2B4DE">
                
 <div align="justify">
    
<br>

<h2 style="color:#8E44AD" align="left"><b><i>Secciones de interés</i></b></h2>
         
         <div style="color:#8E44AD">
             
             <br>
             
             <p style="color:#0a73bb"><?= Html::a('Guía de adopción, importante si quieres adoptar...', ['site/guia'], ['class' => 'btn btn-primary'] )?></p>

<br>

         </div>
            
</div>
 </div>
            </div>
        </div>
     
</td>
</tr>

<br>
<br>
    
</table>
        
    </body>
    
    </html>
