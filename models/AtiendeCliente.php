<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Atiende_cliente".
 *
 * @property int $id_atiende_cliente
 * @property int|null $codigo_cliente
 * @property int|null $codigo_empleado
 */
class AtiendeCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Atiende_cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente', 'codigo_empleado'], 'integer'],
            [['codigo_cliente', 'codigo_empleado'], 'unique', 'targetAttribute' => ['codigo_cliente', 'codigo_empleado']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_atiende_cliente' => 'Id Atiende Cliente',
            'codigo_cliente' => 'Codigo Cliente',
            'codigo_empleado' => 'Codigo Empleado',
        ];
    }
}
