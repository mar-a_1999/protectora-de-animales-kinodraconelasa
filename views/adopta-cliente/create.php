<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adoptacliente */

$this->title = 'Create Adoptacliente';
$this->params['breadcrumbs'][] = ['label' => 'Adoptaclientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adoptacliente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
