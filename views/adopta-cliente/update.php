<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adoptacliente */

$this->title = 'Update Adoptacliente: ' . $model->id_adopta_cliente;
$this->params['breadcrumbs'][] = ['label' => 'Adoptaclientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_adopta_cliente, 'url' => ['view', 'id' => $model->id_adopta_cliente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adoptacliente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
