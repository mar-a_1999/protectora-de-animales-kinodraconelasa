<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear noticia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{pager}\n{items}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'titulo',
            'cuerpo',
            'imagen',
            'adoptado_o_no',
            'fecha_publicacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
