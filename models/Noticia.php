<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property string $titulo
 * @property string|null $cuerpo
 * @property string|null $imagen
 * @property int|null $adoptado_o_no
 * @property string|null $fecha_publicacion
 */
class Noticia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['adoptado_o_no'], 'integer'],
            [['fecha_publicacion'], 'safe'],
            [['titulo'], 'string', 'max' => 600],
            [['cuerpo'], 'string', 'max' => 5000],
            [['imagen'], 'string', 'max' => 100],
            [['titulo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'titulo' => 'Titulo',
            'cuerpo' => 'Cuerpo',
            'imagen' => 'Imagen',
            'adoptado_o_no' => 'Adoptado O No',
            'fecha_publicacion' => 'Fecha Publicacion',
        ];
    }
}
