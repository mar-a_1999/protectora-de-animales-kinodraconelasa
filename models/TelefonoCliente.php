<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Telefono_cliente".
 *
 * @property int $id_telefono_cliente
 * @property int|null $codigo_cliente
 * @property string|null $telefono
 */
class TelefonoCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Telefono_cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_cliente'], 'integer'],
            [['telefono'], 'string', 'max' => 9],
            [['codigo_cliente', 'telefono'], 'unique', 'targetAttribute' => ['codigo_cliente', 'telefono']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_telefono_cliente' => 'Id Telefono Cliente',
            'codigo_cliente' => 'Codigo Cliente',
            'telefono' => 'Telefono',
        ];
    }
}
