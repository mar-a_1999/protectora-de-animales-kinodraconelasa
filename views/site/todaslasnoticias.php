<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = 'Todas las noticias';
$this->params['breadcrumbs'][] = $this->title;

?>
   
<h1 style="color:#8E44AD"><b><i>Todas las noticias</i></b></h1>

<br>

<p>
    <?=    Html::a('Crear noticia', ['noticia/create'], ['class' => 'btn btn-warning']) ?>
</p>

<br>

<div class="row">
<?=    ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_noticia',
            'layout'=>"{pager}\n{items}",
    
]);
?>
    
    
</div>