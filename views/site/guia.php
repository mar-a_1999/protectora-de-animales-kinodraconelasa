<?php

use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = 'Guía de adopción, importante si quieres adoptar...';
$this->params['breadcrumbs'][] = $this->title;

    ?>


<br>
<br>

       <table width="100%" border="0">

           <tr>
               
               <td align="center"><?= Html::img('@web/images/imagen-gato-guia.jpg', ['alt' => 'Error al mostrar la imagen'])?></td>
         
               
               <td align="center"><h1 style="color:#8E44AD"><b><i>Guía de adopción</i></b></h1></td>
              
           
               <td align="center"><?= Html::img('@web/images/imagen-perro-guia.jpg', ['alt' => 'Error al mostrar la imagen'])?></td>
               
          
          </tr>

      </table>

                  <br>
                  <br>
                  <br>
                  
                  
                  <p align="justify">Lo más importante a tener en cuenta a la hora de adoptar un animal, es pararse a pensar si realmente estamos preparados para dar este paso, nosotros y el resto de las personas con las que convivimos. Un animal, no es un juguete, ni un objeto que se pueda tirar, o dejar cuando ya nos cansemos de él, o no lo queramos. Los gatos pueden llegar a vivir 20 años, o más en algunos casos, con lo que será nuestro compañero durante gran parte de nuestra vida, un compañero al que tendremos que atender, alimentar, cuidar y llevar al veterinario para sus revisiones, o enfermedades. Una vez que estamos decididos a dar este gran paso de compartir parte de nuestra vida con un animal, debemos decidir que tipo de mascota se adecúa más a nosotros, bien por tiempo, por necesidades, o por gusto. <strong>Cada animal requiere unas necesidades específicas</strong>, en el caso de gatos, no es necesario sacarles a pasear (de echo no es nada aconsejable hacerlo, sobretodo en la ciudad), ni necesitan salir para hacer sus necesidades, debido a lo cual últimamente es la mascota escogida por muchas personas, que, mayormente por motivos de trabajo, no tienen todo el tiempo que quisieran para esta labor.
                 
                      <br>
                      <br>
                      
                  <p align="justify">¿Si te has decidido por tener una mascota qué deberías hacer ahora?</p>
              
                      <br>
                  
                  <p align="justify"><strong>La mejor opción es adoptar al animal.</strong></p>
                 
                      <br>
                      
                      <p align="justify">Los animales que tenemos para adoptar en las protectoras, suelen ser animales que han sido abandonados, maltratados, nacimientos no deseados por no tener a los animales esterilizados, animales en colonias callejeras que están en peligro, o una mezcla de todas estas causas. <strong>La adopción permite a estos animales salir adelante dándoles una nueva oportunidad de vivir.</strong> Gran parte de los animales que han sido abandonados sufren graves problemas de depresión debidos al abandono, dejándose morir en algunos casos. Para las adopciones <strong>la mejor opción es contactar con las protectoras de la zona</strong>, ellas te informarán de los animales que tienen y <strong>te aconsejarán sobre ellos y sus cuidados</strong>, hay que recordar que cada uno de ellos tiene su propia personalidad y su propio caracter. También se puede ir a las perreras, ya que allí los animales son sacrificados, aunque estén sanos, pasado un periodo de tiempo.</p>
        
                     