<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdoptaSocio */

$this->title = 'Create Adopta Socio';
$this->params['breadcrumbs'][] = ['label' => 'Adopta Socios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adopta-socio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
