<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Cliente".
 *
 * @property int $codigo_cliente
 * @property string|null $direccion
 * @property string|null $dni
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['direccion'], 'string', 'max' => 40],
            [['dni'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_cliente' => 'Codigo Cliente',
            'direccion' => 'Direccion',
            'dni' => 'Dni',
        ];
    }
}
