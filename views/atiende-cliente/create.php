<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AtiendeCliente */

$this->title = 'Create Atiende Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Atiende Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atiende-cliente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
