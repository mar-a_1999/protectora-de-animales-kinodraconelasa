<?php

use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$this->title = 'Adoptados';
$this->params['breadcrumbs'][] = $this->title;

?>

<h1 style="color:#8E44AD"><b><i>Adoptados</i></b></h1>

<br>
        
<table>

    <tr>
        
        <td>

<div align="center" style="border:5px solid #00b3ee;" class="col-lg-12">
                       
               <h3><strong>Toby</strong></h3>
               
               <br>

               <p><?= Html::img('@web/images/perro_adoptado_ pagina_principal.jpg', ['alt' => 'Error al mostrar la imagen'])?></p>
               
               <br>
               
               <h3>Detalles del animal:</h3>
                   
               <br>
                   
                   <div align="center">
                       
                       <p><strong>Nombre: </strong>Toby</p>
                       <p><strong>Fecha de entrada: </strong>20/03/2020</p>
                       <p><strong>Fecha de salida: </strong>8/04/2020</p>
                       <p><strong>Lugar actual: </strong>Local calle Lamara</p>
                       <p><strong>Tipo: </strong>Perro</p>
                       <p><strong>Edad: </strong>4 años</p>
                       <p><strong>Sexo: </strong> Macho</p>
                       <p><strong>Tamaño: </strong> Mediano</p>
                       <p><strong>Raza: </strong> Labrador</p>
                       <p><strong>Vacunado: </strong> Si</p>
                       <p><strong>Esterilizado: </strong> Si</p>
                       
                   </div>
               
               <br>
               <br>
               
               <p>................................................................................................................................................</p>
               
               <br>
               
                   <h3 align="center">Descripción.</h3>
               
                   <p align="center"><strong>ADOPTADO</strong></p>
               
               <p align="justify">En la tarde de ayer, día ocho de abril, una chica de veintiún años y llamada Annabeth se presentó en nuestras instalaciones de la calle Lamara. Se mostró muy interesada por nuestro cachorro, y al cabo de un rato hablando con uno de nuestros empleados, se decantó por acoger al pequeño. Después de unos trámites nuestro querido amigo ya puede disfrutar de su nueva familia.</p>
               
               <br>
               
               </div>
    
        </td>

    </tr>

    <tr>
    
        <td>
               <br>            
               <br>
               
               
                <div align="center" style="border:5px solid #00b3ee;" class="col-lg-12">
                   
                <h3><strong>Simba</strong></h3>
                
                <br>
                    
                <p><?= Html::img('@web/images/jessy gatita adoptado.jpg', ['alt' => 'Error al mostrar la imagen'])?></p>
                   
                <br>
                
                   <h3 align="center">Detalles del animal:</h3>
                   
               <br>
                   
               <div align="center">
                   
                       <p><strong>Nombre: </strong>Simba</p>
                       <p><strong>Fecha de entrada: </strong>13/04/2020</p>
                       <p><strong>Fecha de salida: </strong>13/05/2020</p>
                       <p><strong>Lugar actual: </strong>Local calle Giralda</p>
                       <p><strong>Tipo: </strong>Gato</p>
                       <p><strong>Edad: </strong>2 años</p>
                       <p><strong>Sexo: </strong> Macho</p>
                       <p><strong>Tamaño: </strong> Pequeño</p>
                       <p><strong>Raza: </strong> común</p>
                       <p><strong>Vacunado: </strong> Si</p>
                       <p><strong>Esterilizado: </strong> Si</p>
                       
              </div>
               
               <br>
               <br>
               
               <p>................................................................................................................................................</p>
               
               <br>
                
                <h3 align="center">Descripción.</h3>
                
                <p align="center"><strong>ADOPTADO</strong></p>
                
                <p align="justify">Simba ha sido adoptado durante esta misma mañana al ser acogido por los padres de una familia numerosa, a la cual, les encantó el gatito nada más verle.</p> 
                
                <br>
                
                </div>
     
               </td>

        </tr>
               
</table>