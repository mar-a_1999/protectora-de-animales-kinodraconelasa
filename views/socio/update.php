<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Socio */

$this->title = 'Editar socio: ' . $model->codigo_socio;
$this->params['breadcrumbs'][] = ['label' => 'Socios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_socio, 'url' => ['view', 'id' => $model->codigo_socio]];
$this->params['breadcrumbs'][] = 'Editar socio';
?>
<div class="socio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
