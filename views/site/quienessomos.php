<?php

use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$this->title = '¿Quiénes somos?';
$this->params['breadcrumbs'][] = $this->title;

?>

 <table width="100%" border="0">

           <tr>

                   <td align="left">
                   
                   <h1 style="color:#8E44AD"><b><i>¿Quiénes somos?</i></b></h1>
                   
                   <br>

                   <p align="justify"><strong>Kinodraconelasa</strong> es una joven <strong>asociación sin ánimo de lucro ni fines económicos</strong>, creada en enero de 2020.</p> 
                   
                   <p>Estamos <strong>comprometidos con los animales</strong> más necesitados. Por ello <strong>nos ponemos en marcha para buscarles un hogar</strong> definitivo. <strong>Cada uno de nuestros animales</strong> es como si fuera uno más de <strong>la familia</strong></p>                  
                   
                   <br>
                   
                   <h3 align="justify">En la actualidad estos son nuestros objetivos:</h3>

                   <br>
                   
                   <p align="justify"><strong>Recogemos a los animales más desfavorecidos y</strong> que realmente se encuentren <strong>en peligro.</strong> 
                       
                   <br>
                       
                   <p><strong>Difundimos</strong> a nuestros <strong>animales para encontrarles adoptantes y </strong> al mismo tiempo <strong>ayudamos a otras asociaciones como la nuestra</strong>, en la misma tarea.</p>

                   <br>
                   
                   <h3 align="justify">Es por esto por lo que necesitamos vuestra ayuda por mínima que sea, trabajamos sin ayudas y necesitamos colaboración para:</h3>

                   <br>
                   
                   <p align="justify"><strong>Alimentar</strong> a nuestros <strong>animales.</strong></p>

                   <br>
                   
                   <p align="justify"><strong>Sufragar</strong> todos los <strong>gastos veterinarios</strong>, como desparasitaciones, vacunas, esterilizaciones, y tratamientos para los que vengan enfermos o heridos por maltrato físico y/o psíquico.</p>

                   <br>
                   
                   <p align="justify"><strong>Hacer nuevas jaulas</strong> para nuestros animales cuando enferman o llegan nuevos. <strong>Tenemos que renovar todas</strong> las que tenemos, <strong>bien porque están rotas o</strong> porque <strong>no son adecuadas para</strong> los <strong>animales.</strong></p>

                   <br>
                   
                   <p align="justify"><strong>Realizar reformas</strong> en nuestros locales <strong>que mejoren la estancia de</strong> nuestros <strong>animales.</strong></p>

                   <br>
                   
                   <p align="center"><img src="/ProtectoraAnimalesKinodraconelasaFinDeCiclo/web/images/interrogante_quienes_somos.jpg" alt="Error al mostrar la imagen"></p>
                   
               </td>
           
         </tr>
         
</table> 